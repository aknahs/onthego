package com.wipjam.onthego;

import java.io.ByteArrayOutputStream;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.sonyericsson.extras.liveware.aef.control.Control;
import com.sonyericsson.extras.liveware.aef.widget.Widget;
import com.sonyericsson.extras.liveware.extension.util.widget.BaseWidget;

public class ImageWidget extends BaseWidget {

	public static final String KEY_1_2 = "1_2";
	public static final int WIDTH_1_2 = 86;
	public static final int HEIGHT_1_2 = 34;

	public static final String KEY_2_5 = "2_5";
	public static final int WIDTH_2_5 = 215;
	public static final int HEIGHT_2_5 = 68;

	private SharedPreferences mPreferences;

	private final OnSharedPreferenceChangeListener mPreferenceListener = new OnSharedPreferenceChangeListener() {
		@Override
		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			if (key.compareTo("direction") == 0) {
				setNewImage(DIRECTION_UP);
			}
		}
	};

	public ImageWidget(WidgetBundle bundle) {
		super(bundle);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getWidth() {
		return WIDTH_2_5;
	}

	@Override
	public int getHeight() {
		return HEIGHT_2_5;
	}

	@Override
	public int getPreviewUri() {
		return R.drawable.preview_2_5;
	}

	@Override
	public int getName() {
		return R.string.widget_name_2_5;
	}

	@Override
	public void onCreate() {
		mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
	}

	@Override
	public void onStartRefresh() {
		// Widget is now visible. Start listening for changes.
		mPreferences
				.registerOnSharedPreferenceChangeListener(mPreferenceListener);

	}

	@Override
	public void onStopRefresh() {
		// Widget is no longer visible. Stop listening for changes.
		mPreferences
				.unregisterOnSharedPreferenceChangeListener(mPreferenceListener);
	}

	public void onTouch(int type, int x, int y) {
		// User has tapped the widget. Open the control part of the extension.
		Intent intent = new Intent(Control.Intents.CONTROL_START_REQUEST_INTENT);
		intent.putExtra(Control.Intents.EXTRA_AEA_PACKAGE_NAME,
				mContext.getPackageName());
		sendToHostApp(intent);
	}

	@Override
	public void onObjectClick(int type, int layoutReference) {
		// User has tapped a widget object. Open the control part of the
		// extension.
		Intent intent = new Intent(Control.Intents.CONTROL_START_REQUEST_INTENT);
		intent.putExtra(Control.Intents.EXTRA_AEA_PACKAGE_NAME,
				mContext.getPackageName());
		sendToHostApp(intent);
	}

	public void setNewImage() {
		Integer widgetText = mPreferences.getInt("direction", DIRECTION_UP);
		setNewImage(widgetText);
	}

	public static final int DIRECTION_UP = 0;
	public static final int DIRECTION_DOWN = 1;
	public static final int DIRECTION_LEFT = 2;
	public static final int DIRECTION_RIGHT = 3;
	public static final int DIRECTION_DOWN_RIGHT = 4;
	public static final int DIRECTION_UP_RIGHT = 5;
	public static final int DIRECTION_UP_LEFT = 6;
	public static final int DIRECTION_DOWN_LEFT = 7;

	public void setNewImage(Integer direction) {
		Drawable drawable;

		switch (direction) {
		case DIRECTION_UP:
			drawable = mContext.getResources().getDrawable(R.drawable.up);
			break;
		case DIRECTION_DOWN:
			drawable = mContext.getResources().getDrawable(R.drawable.down);
			break;
		case DIRECTION_LEFT:
			drawable = mContext.getResources().getDrawable(R.drawable.left);
			break;
		case DIRECTION_RIGHT:
			drawable = mContext.getResources().getDrawable(R.drawable.right);
			break;
		case DIRECTION_DOWN_RIGHT:
			drawable = mContext.getResources()
					.getDrawable(R.drawable.downright);
			break;
		case DIRECTION_UP_RIGHT:
			drawable = mContext.getResources().getDrawable(R.drawable.upright);
			break;
		case DIRECTION_UP_LEFT:
			drawable = mContext.getResources().getDrawable(R.drawable.upleft);
			break;
		case DIRECTION_DOWN_LEFT:
			drawable = mContext.getResources().getDrawable(R.drawable.downleft);
			break;
		default:
			drawable = mContext.getResources().getDrawable(R.drawable.up);
			break;
		}

		Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		byte[] bitmapdata = stream.toByteArray();

		// Prepare a bundle with the string and the resource id in the layout
		// that shall be updated.
		Bundle bundle = new Bundle();
		bundle.putString(Widget.Intents.EXTRA_AEA_PACKAGE_NAME,
				mContext.getPackageName());
		bundle.putInt(Widget.Intents.EXTRA_LAYOUT_REFERENCE,
				R.id.widget_image_box_view);
		bundle.putByteArray(Widget.Intents.EXTRA_WIDGET_IMAGE_DATA, bitmapdata);

		Bundle[] layoutData = new Bundle[1];
		layoutData[0] = bundle;
		showLayout(R.layout.image_layout, layoutData);

	}

	public int getLayoutId() {
		return R.layout.image_layout;
	}

}
