package com.wipjam.onthego.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationClient;
import com.wipjam.onthego.ImageActivity;
import com.wipjam.onthego.ImageControl;
import com.wipjam.onthego.ImageHub;
import com.wipjam.onthego.bicing.MyJsonParser;
import com.wipjam.onthego.bicing.MyXmlParser;
import com.wipjam.onthego.bicing.Station;
//http://opendata.bcn.cat/opendata/cataleg/TRANSPORT/bicing/#
//http://wservice.viabicing.cat/getstations.php?v=1
//http://developer.android.com/google/play-services/setup.html
//https://developer.android.com/training/location/retrieve-current.html#DefineCallbacks

//import com.google.android.gms.common.api.GoogleApiClient;

public class Tconnector implements Runnable {
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String TAG = "TCON";
	private LocationClient mLocationClient = null;
	private Context mContext;

	// In the future there is probably no need to use this API as it takes more
	// data consumption
	private static final String GET_NEAR_FORMAT = "http://barcelonaapi.marcpous.com/bicing/nearstation/latlon/%f/%f/1.json";

	/*
	 * Define a request code to send to Google Play services This code is
	 * returned in Activity.onActivityResult
	 */
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private ArrayList<Station> mKnownStations = null;
	private ArrayList<Station> mNearStations = null;

	public Tconnector(LocationClient loc, Context ctx) {
		mLocationClient = loc;
		mContext = ctx;
	}

	// Global variable to hold the current location
	private Location mCurrentLocation;
	// Global variable to hold the previous location
	private Location mPreviousLocation;

	public Location getCurrentKnownLocation() {
		if (!mLocationClient.isConnected() && !mLocationClient.isConnecting())
			return null;
		Location l = mLocationClient.getLastLocation();

		if (l != null) {
			mPreviousLocation = mCurrentLocation;
			mCurrentLocation = l;

		}

		return mCurrentLocation;
	}

	public Location getLastLastKnownLocation() {
		return mPreviousLocation;
	}

	public String getCloseStations(Location loc) {
		String url = String.format(Locale.US, GET_NEAR_FORMAT,
				loc.getLatitude(), loc.getLongitude());
		return getXmlFromUrl(url);

	}

	public String getAllStations() {
		String url = "http://wservice.viabicing.cat/getstations.php?v=1";
		return getXmlFromUrl(url);
	}

	/**
	 * Getting XML from URL making HTTP request
	 * 
	 * @param url
	 */
	public String getXmlFromUrl(String url) {
		String xml = null;

		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			xml = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return XML
		return xml;
	}

	// HTTP GET request
	public StringBuffer httpGet(String url) {
		URL obj;
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			Log.v(TAG, "\nSending 'GET' request to URL : " + url);
			Log.v(TAG, "Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));

			String inputLine;
			StringBuffer response = new StringBuffer(52000);

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();

			// print result
			Log.v(TAG, response.toString());
			return response;

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	ArrayList<Double> partitions = null; //new ArrayList<Integer> partitions();
	HashMap<Double, Integer> arrowIds = new HashMap<Double, Integer>();
	
	public void applyAngleToArrow(double angle) {
		Double a = Double.valueOf(0.0);
		if(partitions == null){
			partitions  = new ArrayList<Double>();
			partitions.add(a);//0
			arrowIds.put(a, ImageControl.DIRECTION_UP);
			a += 45;
			partitions.add(a);//45
			arrowIds.put(a, ImageControl.DIRECTION_UP_RIGHT);
			a += 45;
			partitions.add(a);//90
			arrowIds.put(a, ImageControl.DIRECTION_RIGHT);
			a += 45;
			partitions.add(a);//135
			arrowIds.put(a, ImageControl.DIRECTION_DOWN_RIGHT);
			a += 45;
			partitions.add(a);//180
			arrowIds.put(a, ImageControl.DIRECTION_DOWN);
			a += 45;
			partitions.add(a);//225
			arrowIds.put(a, ImageControl.DIRECTION_DOWN_LEFT);
			a += 45;
			partitions.add(a);//270
			arrowIds.put(a, ImageControl.DIRECTION_LEFT);
			a += 45;
			partitions.add(a);//315
			arrowIds.put(a, ImageControl.DIRECTION_UP_LEFT);
			//a += 45;
			//partitions.add(a);//360
		}
		if (ImageHub.mImageControl != null) {
			Double[] dists = new Double[partitions.size()];
			int i = 0;
			double min = 99999;
			double minAngle = 0; 
			for(Double p : partitions){
				dists[i] = p - angle;
				if(dists[i] < 0)
					dists[i] = dists[i] * (-1);
				if(dists[i] < min){
					min = dists[i];
					minAngle = p; 
				}
			}
			Log.v(TAG, "Closest angle was : " + minAngle + " arrow was " + arrowIds.get(minAngle));
			ImageHub.mImageControl.setNewImage(arrowIds.get(minAngle));
		}
	}

	@Override
	public void run() {
		Location loc;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ((ImageActivity) mContext).registerLocationListener();
		while (true) {
			try {

				Log.v(TAG, "-------------ALL STATIONS-----------------");
				String stations = getAllStations(); // xml
				Thread.sleep(5000);
				mKnownStations = MyXmlParser.getStations(stations);
				Log.v(TAG, "---------------LOCATION-------------------");
				loc = getCurrentKnownLocation();
				// loc = ((ImageActivity) mContext).mLastLocation;
				if (loc != null && mKnownStations != null) {
					Log.v(TAG,
							"lat : " + loc.getLatitude() + " lon : "
									+ loc.getLongitude());
					// Thread.sleep(10000);
					Log.v(TAG, "-------------NEAR STATIONS----------------");
					String near = getCloseStations(loc); // json
					Thread.sleep(5000);
					mNearStations = MyJsonParser.parse(near);

					Log.v(TAG, "-------------BEARING----------------");
					Station nearest = mNearStations.get(0);
					Log.v(TAG, "My bearing : " + loc.getBearing());

					Location l = new Location("any string");
					l.setLatitude(nearest.lat);
					l.setLongitude(nearest.longi);

					Station sWithBikeInfo = nearest;
					Log.v(TAG, "Nearest id = " + nearest.id);
					for (Station s : mKnownStations) {
						Log.v(TAG, "Compare with id = " + s.id);
						if (s.id.equals(nearest.id)) {
							Log.v(TAG, "found station : " + s);
							sWithBikeInfo = s;
							break;
						}
					}

					Log.v(TAG, "My bearing to location : " + loc.bearingTo(l));

					/*
					 * GeoPoint newCurrent = new GeoPoint(59529200, 18071400);
					 * Location current = new Location("reverseGeocoded");
					 * current.setLatitude(newCurrent.getLatitudeE6() / 1e6);
					 * current.setLongitude(newCurrent.getLongitudeE6() / 1e6);
					 * current.setAccuracy(3333); current.setBearing(333);
					 */
					applyAngleToArrow(loc.bearingTo(l));

					((ImageActivity) mContext).runOnUiThread(new ToastRunnable(
							mContext, sWithBikeInfo));

				} else {
					Log.v("xxx", "location = null");
				}

				/*
				 * if (ImageHub.mImageControl != null) { Integer i = (int)
				 * (Math.random() * 7); ImageHub.mImageControl.setNewImage(i); }
				 */
				Thread.sleep(25000);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.v("xxx", "Exception : " + e);
				e.printStackTrace();
			}
		}

	}
}
