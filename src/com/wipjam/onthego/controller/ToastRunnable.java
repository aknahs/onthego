package com.wipjam.onthego.controller;

import android.content.Context;
import android.widget.Toast;

import com.wipjam.onthego.ImageActivity;
import com.wipjam.onthego.bicing.Station;

public class ToastRunnable implements Runnable {
	private Context mContext;
	private Station mStation;
	
	public ToastRunnable(Context c, Station s){
		mContext = c;
		mStation = s;
	}
	public void run() {
		Toast.makeText(((ImageActivity) mContext),
				"Nearest:" + mStation, Toast.LENGTH_LONG).show();
	}
}
