package com.wipjam.onthego;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.sonyericsson.extras.liveware.aef.widget.Widget;
import com.sonyericsson.extras.liveware.extension.util.control.ControlExtension;

public class ImageControl extends ControlExtension {

	private final SharedPreferences mPreferences;
	private static Context mCtx;

	private final OnSharedPreferenceChangeListener mPreferenceListener = new OnSharedPreferenceChangeListener() {
		@Override
		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			if (key.compareTo("direction") == 0) {
				setNewImage(sharedPreferences.getInt(key, DIRECTION_UP));
			}
		}
	};

	public void setNewImage() {
		Integer widgetText = mPreferences.getInt("direction", DIRECTION_UP);
		setNewImage(widgetText);
	}

	public static final int DIRECTION_UP = 0;
	public static final int DIRECTION_DOWN = 1;
	public static final int DIRECTION_LEFT = 2;
	public static final int DIRECTION_RIGHT = 3;
	public static final int DIRECTION_DOWN_RIGHT = 4;
	public static final int DIRECTION_UP_RIGHT = 5;
	public static final int DIRECTION_UP_LEFT = 6;
	public static final int DIRECTION_DOWN_LEFT = 7;
	
	public void setNewImage(Integer direction) {
		Drawable drawable;
		
		switch (direction) {
			case DIRECTION_UP:
				drawable = mContext.getResources().getDrawable(R.drawable.up);
				break;
			case DIRECTION_DOWN:
				drawable = mContext.getResources().getDrawable(R.drawable.down);		
				break;
			case DIRECTION_LEFT:
				drawable = mContext.getResources().getDrawable(R.drawable.left);
				break;
			case DIRECTION_RIGHT:
				drawable = mContext.getResources().getDrawable(R.drawable.right);
				break;
			case DIRECTION_DOWN_RIGHT:
				drawable = mContext.getResources().getDrawable(R.drawable.downright);
				break;
			case DIRECTION_UP_RIGHT:
				drawable = mContext.getResources().getDrawable(R.drawable.upright);
				break;
			case DIRECTION_UP_LEFT:
				drawable = mContext.getResources().getDrawable(R.drawable.upleft);
				break;
			case DIRECTION_DOWN_LEFT:
				drawable = mContext.getResources().getDrawable(R.drawable.downleft);
				break;
			default:
				drawable = mContext.getResources().getDrawable(R.drawable.up);
				break;
		}

		Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
	    ByteArrayOutputStream stream = new ByteArrayOutputStream();
	    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
	    byte[] bitmapdata = stream.toByteArray();
	    
        // Prepare a bundle with the string and the resource id in the layout
        // that shall be updated.
            Bundle bundle = new Bundle();
            bundle.putString(Widget.Intents.EXTRA_AEA_PACKAGE_NAME,
            		mContext.getPackageName());
            bundle.putInt(Widget.Intents.EXTRA_LAYOUT_REFERENCE,
                    R.id.widget_image_box_view);
            bundle.putByteArray(Widget.Intents.EXTRA_WIDGET_IMAGE_DATA, bitmapdata);
        
            Bundle[] layoutData = new Bundle[1];
            layoutData[0] = bundle;
        showLayout(R.layout.image_layout, layoutData);

    }

	ImageControl(Context context, String hostAppPackageName) {
		super(context, hostAppPackageName);
		mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
	}

	public void onActiveLowPowerModeChange(boolean lowPowerModeOn) {
		if (lowPowerModeOn) {
			// Adapt your extension layout when the Low Power mode is
			// enabled
		} else {
			// Adapt your extension layout to the Low Power mode is
			// disabled
		}
	}

	@Override
	public void onResume() {
		// Control is now visible. Start listening for changes.
		mPreferences
				.registerOnSharedPreferenceChangeListener(mPreferenceListener);

		// Show the text that is saved in the preferences.
		setNewImage();
	}

	@Override
	public void onPause() {
		// Control is no longer visible. Stop listening for changes.
		mPreferences
				.unregisterOnSharedPreferenceChangeListener(mPreferenceListener);
	}
}
