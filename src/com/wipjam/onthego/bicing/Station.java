package com.wipjam.onthego.bicing;

public class Station {
	public Integer id;
	public Double lat;
	public Double longi;
	public String street;
	public Integer height;
	public Integer num;
	public Integer slots;
	public Integer bikes;

	@Override
	public String toString() {
		return "[" + id + "][" + lat + "," + longi + "][" + street + ","
				+ num + "][" + slots + " / " + bikes + "]";
	}
}