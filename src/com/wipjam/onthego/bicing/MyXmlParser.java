package com.wipjam.onthego.bicing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringBufferInputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

@SuppressLint("NewApi")
public class MyXmlParser {

	// We don't use namespaces
	private static final String ns = null;

	static ArrayList<Station> mStations = null;

	private static void writeToFile(String data) {
		try {
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(sdCard.getAbsolutePath() + "/onthego");
			dir.mkdirs();
			File file = new File(dir, "text.xml");

			FileOutputStream f = new FileOutputStream(file);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(f);
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public static ArrayList<Station> getStations(String buf) {

		try {
			return parse(new StringBufferInputStream(buf));
			//writeToFile(buf);
			//for (Station s : mStations) {
			//	Log.v("xxx", "stations stored : " + s);
			//}
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			Log.v("xxx", "IOException error");
			e.printStackTrace();
		}
		return null;
	}

	public static ArrayList<Station> parse(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readFeed(parser);
		} finally {
			in.close();
		}
	}

	private static void skip(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

	private static ArrayList<Station> readFeed(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "bicing_stations");

		if (mStations == null)
			mStations = new ArrayList<Station>();
		mStations.clear();

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			// Starts by looking for the entry tag
			if (name.equals("station")) {
				readEntry(parser);
			} else {
				skip(parser);
			}
		}
		return mStations;
	}

	static StringBuffer buf = new StringBuffer(
			"<bicing_stations><updatetime><![CDATA[1393293006]]></updatetime><station><id>1</id><lat>41.3979520</lat><long>2.18004200</long><street><![CDATA[Gran Via Corts Catalanes]]></street>  <height>21</height><streetNumber>760</streetNumber><nearbyStationList>24,369,387,426</nearbyStationList><status>OPN</status><slots>0</slots><bikes>23</bikes></station><station><id>2</id><lat>41.3942720</lat><long>2.17516900</long><street><![CDATA[Plaza Tetu&aacute;n]]></street><height>21</height><streetNumber>8</streetNumber><nearbyStationList>360,368,387,414</nearbyStationList><status>OPN</status><slots>14</slots><bikes>13</bikes></station></bicing_stations>");

	// Parses the contents of an entry. If it encounters a title, summary, or
	// link tag, hands them off
	// to their respective "read" methods for processing. Otherwise, skips the
	// tag.
	private static void readEntry(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		parser.require(XmlPullParser.START_TAG, ns, "station");

		Station stat = new Station();

		mStations.add(stat);

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			/*
			 * if (name.equals("station")) { if(stat != null){ Log.v("xxx",
			 * "station stored : " + stat); } stat = new Station();
			 * mStations.add(stat); } else
			 */
			if (name.equals("id")) {

				stat.id = readID(parser);
			} else if (name.equals("lat")) {

				stat.lat = readLat(parser);
			} else if (name.equals("long")) {

				stat.longi = readLon(parser);
			} else if (name.equals("street")) {

				stat.street = readStreet(parser);
			} else if (name.equals("streetNumber")) {

				stat.num = readNumber(parser);
			} else if (name.equals("slots")) {

				stat.slots = readSlots(parser);
			} else if (name.equals("bikes")) {

				stat.bikes = readBikes(parser);
			} else {
				skip(parser);
			}
		}

	}

	public static Integer readID(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "id");
		Integer id = Integer.valueOf(readText(parser));
		parser.require(XmlPullParser.END_TAG, ns, "id");
		return id;
	}

	public static Double readLat(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "lat");
		Double id = Double.valueOf(readText(parser));
		parser.require(XmlPullParser.END_TAG, ns, "lat");
		return id;
	}

	public static Double readLon(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "long");
		Double id = Double.valueOf(readText(parser));
		parser.require(XmlPullParser.END_TAG, ns, "long");
		return id;
	}

	public static String readStreet(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "street");
		String id = readText(parser);
		parser.require(XmlPullParser.END_TAG, ns, "street");
		return id;
	}

	public static Integer readNumber(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		Integer id;
		parser.require(XmlPullParser.START_TAG, ns, "streetNumber");
		String msg = readText(parser);
		try {
			id = Integer.valueOf(msg);
			parser.require(XmlPullParser.END_TAG, ns, "streetNumber");
			return id;
		} catch (NumberFormatException e) {
			//Log.v("xxx", "Could not read street number : " + msg);
		}

		return -1;
	}

	// Processes title tags in the feed.
	private static Integer readSlots(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "slots");
		Integer slots = Integer.valueOf(readText(parser));
		parser.require(XmlPullParser.END_TAG, ns, "slots");
		return slots;
	}

	// Processes link tags in the feed.
	private static Integer readBikes(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, "bikes");
		Integer slots = Integer.valueOf(readText(parser));
		parser.require(XmlPullParser.END_TAG, ns, "bikes");
		return slots;
	}

	// For the tags title and summary, extracts their text values.
	private static String readText(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}
}
