package com.wipjam.onthego.bicing;

import java.util.ArrayList;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MyJsonParser {
	
	public static ArrayList<Station> mStations = new ArrayList<Station>();
	
	public static ArrayList<Station> parse(String jsonLine) {
		Station station;
		mStations.clear();
		
		JsonParser parser = new JsonParser();
	    JsonElement jelement = parser.parse(jsonLine);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    
	    jobject = jobject.getAsJsonObject("data");
	    
	    JsonArray jarray = jobject.getAsJsonArray("nearstations");
	    
	    for(JsonElement el : jarray){
	    	//station
	    	station = new Station();
	    	jobject = el.getAsJsonObject();
	    	try{
	    	station.id = Integer.valueOf(jobject.get("id").toString().replace("\"", ""));
	    	station.lat = Double.valueOf(jobject.get("lat").toString().replace("\"", ""));
	    	station.longi = Double.valueOf(jobject.get("lon").toString().replace("\"", ""));
	    	station.street = jobject.get("name").toString().replace("\"", "");
	    	station.num = -1;
	    	station.slots = -1;
	    	station.bikes = -1;
	    	mStations.add(station);
	    	//Log.v("xxx", "inserting station : " + station);
	    	}
	    	catch(NumberFormatException e){
	    		Log.v("xxx", "Error, could not get Integer (or the lat/long doubles) : " + jobject.get("id").toString());
	    	}
	    }
	    return mStations;
	}
}
